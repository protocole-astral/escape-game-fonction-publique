#!/usr/bin/node
console.log("concours_puzzle.js");
export default function concours_puzzle() {
  
  // Base puzzle object
  let concours_puzzle = {
    "signal_memory":[]
  };
  
  concours_puzzle.process = function(signal,game,matrix_port) {

    // Validate player sequence
    if (signal == '#'){
     
      // Player win 
      if (
        this.signal_memory[0] == '1'
        && this.signal_memory[1] == '1'
        && this.signal_memory[2] == '1'
        && this.signal_memory[3] == '1'
      ) {
        matrix_port.write(Buffer.from('S'));
        this.signal_memory = [];
        return 1;
      
      // Player fail
      } else {
        matrix_port.write(Buffer.from('X'));
        this.signal_memory = [];
        // [10] B fail
        return 10;

      }

    // Memorize player choice in sequence
    } else if (signal > -1) {
      this.signal_memory.push(signal);
      matrix_port.write(Buffer.from('B'));
      console.log(this.signal_memory);
    }
    return 0;
  };

  return concours_puzzle;

}
