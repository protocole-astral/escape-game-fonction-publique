#!/usr/bin/node
console.log("bureau_puzzle.js");
export default function bureau_puzzle() {
  
  // Base puzzle object
  let bureau_puzzle = {
    "signal_memory":[]
  };
  
  bureau_puzzle.process = function(signal,game,matrix_port) {

    // Validate player sequence
    if (signal == '#'){
     
      // Player win 
      if (
        this.signal_memory[0] == '3'
        && this.signal_memory[1] == '4'
      ) {
        matrix_port.write(Buffer.from('S'));
        this.signal_memory = [];
        return 1;
      
      // Player fail
      } else {
        matrix_port.write(Buffer.from('X'));
        this.signal_memory = [];
        // [23] E fail 1
        // [24] E fail 2
        // [25] E fail 3
        return [23,24,25][Math.floor(Math.random()*3)];

      }

    // Memorize player choice in sequence
    } else if (signal > -1) {
      this.signal_memory.push(signal);
      matrix_port.write(Buffer.from('B'));
      console.log(this.signal_memory);
    }
    return 0;
  };

  return bureau_puzzle;

}
