#!/usr/bin/node

// Not game
import { SerialPort, ReadlineParser } from 'serialport'; 
import {playAudioFile} from 'audic';
                                                              
// Game                                                       
import matrix_puzzle from './matrix_puzzle.js';
import map_puzzle from './map_puzzle.js';
import concours_puzzle from './concours_puzzle.js';
import code_emploi_puzzle from './code_emploi_puzzle.js';
import bureau_puzzle from './bureau_puzzle.js';
import metier_puzzle from './metier_puzzle.js';
import get_audio_list from './audio_list.js';

/* * * * * * * *
 * CONTROLLERS *
 * * * * * * * */

// Connect the two arduino to two different ports
const port0 = new SerialPort({ 
    path: '/dev/ttyUSB0', 
    baudRate: 9600,
});
const port1 = new SerialPort({ 
    path: '/dev/ttyUSB1', 
    baudRate: 9600,
});

// Declare a matrix and a main port
let matrix_port = false;
let main_port = false;

// Check wich port is matrix and which one is main
const parser0 = new ReadlineParser();
port0.pipe(parser0);
parser0.on('data', data =>{
  if (!matrix_port && data == -1) {
    main_port = port0;
    matrix_port = port1;
    console.log("port0 is main_port");
  }
  if (main_port == port0) {
    main(data.slice(0,-1),game);
  }
});
const parser1 = new ReadlineParser();
port1.pipe(parser1);
parser1.on('data', data =>{
  if (!matrix_port && data == -1) {
    main_port = port1;
    matrix_port = port0;
    console.log("port1 is main_port");
  }
  if (main_port == port1) {
    main(data.slice(0,-1),game);
  }
});

/* * * * *
 * GAME  *
 * * * * */
let audio_list = get_audio_list();
let game = {
  "state":0,
};

let puzzle = undefined;
let audio_playing = false;
let time = 0;
let indice_0 = false;
let indice_1 = false;
let indice_2 = false;
let indice_0_lit = false;
let indice_1_lit = false;
let indice_2_lit = false;
let unallowed_signals = ['50',50,'51',51,'49',49];
let matrix_mode = false;
let last_message = -1;
let message_playing = false;
let sound_playing = false;
let music_playing = audio_list[31];

function reboot_game(){
  game = {
    "state":0,
  };
  puzzle = undefined;
  audio_playing = false;
  time = 0;
  indice_0 = false;
  indice_1 = false;
  indice_2 = false;
  indice_0_lit = false;
  indice_1_lit = false;
  indice_2_lit = false;
  main_port.write('4');
  unallowed_signals = ['50',50,'51',51,'49',49];
  if (matrix_mode) matrix_port.write('D');
  matrix_mode = false;
  last_message = -1;
  if (message_playing) sound_playing.pause();
  message_playing = false;
  sound_playing = false;
  if(music_playing) music_playing.pause();
  music_playing = false;
  audio_list = get_audio_list();
}
function main(signal,game){
  if (signal == 'starting') {
    reboot_game();
  }
  if (unallowed_signals.includes(signal)) return;
  //console.log(signal);
  time ++;
  if (message_playing && !matrix_mode) {
    // yellow when message playing, red if input when message playing
    if (signal != -1) matrix_port.write(Buffer.from('X'));
    if (!(time%1)) matrix_port.write(Buffer.from(signal == -1 ? 'Y' : 'X'));
    return;
  }
  if (indice_0) {
    // TODO diode 1 marche pas
    if (signal == -1 && !indice_0_lit){
      indice_0_lit = true;
      main_port.write('1');
    }
    if (signal == 'B1') {
      quick_message(9);
    }
  }
  if (indice_1) {
    // TODO diode 1 marche pas
    if (signal == -1 && !indice_1_lit){
      indice_1_lit = true;
      main_port.write('2');
    }
    if (signal == 'B2') {
      quick_message(17);
    }
  }
  if (indice_2) {
    // TODO diode 1 marche pas
    if (signal == -1 && !indice_2_lit){
      indice_2_lit = true;
      main_port.write('3');
    }
    if (signal == 'B3') {
      quick_message(22);
    }
  }
  
  // Game start
  if (game.state == 0){
    console.log(signal);
    // Player(s) must press start button to start the game
    // TODO CHANGE TO BIG RED BUTTON
    if (signal == 'stopping') {
      // [0] intro jeu
      message(0);
      // musique
      //audio_list[28].play();
      game.state = 1;
      return;
    }
  }

  // Manual reading phase
  if (game.state == 1){
    // TODO # or * ???
    if (signal == '#') {
      // [1] premier message Camille, énoncé énigme A
      message(1);
      game.state = 2;
      puzzle = map_puzzle();
      return;
    }
  }
  
  // ENIGME A (ville)
  if (game.state == 2){
    puzzle = puzzle == undefined ? map_puzzle() : puzzle;
    let puzzle_state = puzzle.process(signal,game,matrix_port);
    // musique
    audio_list[31].play();
    if (puzzle_state == 1) {
      // SUCESS
      message(8);
      game.state = 3;
      return;
    } else if (puzzle_state > 1){
      quick_message(puzzle_state);
    }
  }

  // TRANSITION A - B
  if (game.state == 3) {
    if (!message_playing) {
      // [9] INDICE 0, énoncé énigme B
      message(9);
      game.state = 4;
      puzzle = concours_puzzle();
      indice_0 = true;
      return;
    }
  }
  
  // ENIGME B (concours)
  if (game.state == 4) {
    puzzle = puzzle == undefined ? concours_puzzle() : puzzle;
    let puzzle_state = puzzle.process(signal,game,matrix_port);
    if (puzzle_state == 1) {
      // SUCESS
      // [11] B success, énoncé énigme C
      message(11);
      puzzle = matrix_puzzle();
      game.state = 5;
      return;
    } else if (puzzle_state > 1){
      quick_message(puzzle_state);
    }
  }
  
  // ENIGME C (batiment)
  if (game.state == 5) {
    puzzle = puzzle == undefined ? matrix_puzzle() : puzzle;
    // musique
    audio_list[31].play();
    if (!matrix_mode){
      matrix_mode = true;
      matrix_port.write('D');
    }
    if (!message_playing) {
      let puzzle_state = puzzle.process(signal,game,matrix_port,time);
      if (puzzle_state == 1) {
        matrix_mode = false;
        game.state = 6;
        matrix_port.write('D');
        puzzle = false;
        // SUCESS
        // [19] C success
        message(19);
        return;
      } else if (puzzle_state > 1){
        quick_message(puzzle_state);
      }
    }
  }
  
  // TRANSITION C - D
  if (game.state == 6) {
    if (!message_playing) {
      // [20] INDICE 1, énoncé énigme D
      message(20);
      game.state = 7;
      puzzle = code_emploi_puzzle();
      indice_1 = true;
      return;
    }
  }
  
  // ENIGME D (code emploi)
  if (game.state == 7) {
    puzzle = puzzle == undefined ? code_emploi_puzzle() : puzzle;
    let puzzle_state = puzzle.process(signal,game,matrix_port);
    if (puzzle_state == 1) {
      // SUCESS
      // [22] D success, énoncé énigme E
      message(22);
      puzzle = bureau_puzzle();
      game.state = 8;
      return;
    } else if (puzzle_state > 1){
      quick_message(puzzle_state);
    }
  }
  
  // ENIGME E (bureau)
  if (game.state == 8) {
    puzzle = puzzle == undefined ? bureau_puzzle() : puzzle;
    let puzzle_state = puzzle.process(signal,game,matrix_port);
    // musique
    audio_list[31].play();
    if (puzzle_state == 1) {
      // SUCESS
      // [26] E success
      message(26);
      //puzzle = bureau_puzzle();
      game.state = 9;
      return;
    } else if (puzzle_state > 1){
      quick_message(puzzle_state);
    }
  }
  
  // TRANSITION E - F
  if (game.state == 9) {
    if (!message_playing) {
      // [27] INDICE 2, énoncé énigme F
      message(27);
      game.state = 10;
      puzzle = metier_puzzle();
      indice_2 = true;
      return;
    }
  }
  
  // ENIGME F (choix métier)
  if (game.state == 10) {
    puzzle = puzzle == undefined ? metier_puzzle() : puzzle;
    let puzzle_state = puzzle.process(signal,game,matrix_port);
    if (puzzle_state == 1) {
      // SUCESS
      // [30] Épilogue
      message(30);
      // musique
      //audio_list[31].play();
      music_playing.pause();
      game.state = 11;
      return;
    } else if (puzzle_state > 1){
      quick_message(puzzle_state);
    }
  }

  // Listen again
  if (signal.includes('*') && last_message > -1){
    message(last_message);
  }
  
}

function message(id) {
  if (message_playing) return;
  last_message = id;
  let audio = audio_list[id];
  sound_playing = audio;
  audio.play();
  message_playing = true;

  // green(sucess) when message start
  matrix_port.write(Buffer.from('S'));
  audio.addEventListener('ended', () => {
      
    // green(sucess) when message stop
    matrix_port.write(Buffer.from('S'));
    message_playing = false;
  });
}
function quick_message(id) {
  if (message_playing) return;
  let audio = audio_list[id];
  sound_playing = audio;
  audio.play();
  message_playing = true;

  audio.addEventListener('ended', () => {
    message_playing = false;
  });
}
