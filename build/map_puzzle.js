#!/usr/bin/node
console.log("map_puzzle.js");
export default function map_puzzle() {
  
  // Base puzzle object
  let map_puzzle = {
    "signal_memory":[]
  };
  
  map_puzzle.process = function(signal,game,matrix_port) {

    // Validate player sequence
    if (signal == '#'){
     
      // Player win 
      if (
        this.signal_memory.length == 2
        && this.signal_memory.includes('3')
        && this.signal_memory.includes('0')
      ) {
        matrix_port.write(Buffer.from('S'));
        this.signal_memory = [];
        return 1;
      
      // Player fail
      } else {
        matrix_port.write(Buffer.from('X'));
  // [2] A fail : rue en travaux 1
  // [3] A fail : rue en travaux 2
  // [4] A fail : ambulance
  // [5] A fail : rue bloquée
  // [6] A fail : rue bloquée éboueurs
  // [7] A fail : trajet absurde
        
        // Identity error source
        let error_clues = []
        // travaux
        if (this.signal_memory.includes('1')) error_clues = error_clues.concat([0,1]);
        // travaux + ambulance
        if (this.signal_memory.includes('2')) error_clues = error_clues.concat([0,1,2]);
        // travaux + camion
        if (this.signal_memory.includes('4')) error_clues = error_clues.concat([0,1,3]);
        // camion
        if (this.signal_memory.includes('5')) error_clues = error_clues.concat([3]);
        let error_clues_audio = [
          2,
          3,
          4,
          6
        ];
        
    
        let clue = error_clues_audio[
          error_clues[Math.floor(
            Math.random()*error_clues.length
          )]
        ];
        
        // Absurd choice
        if (this.signal_memory.length > 2 || this.signal_memory.includes('*') || !error_clues.length || this.signal_memory[0] == this.signal_memory[1]) {
        
          this.signal_memory = [];
          return 7;
        } else {

          this.signal_memory = [];
          return clue;
        }

      }

    // Memorize player choice in sequence
    } else if (signal > -1) {
      this.signal_memory.push(signal);
      matrix_port.write(Buffer.from('B'));
      console.log(this.signal_memory);
    }
    return 0;
  };

  return map_puzzle;

}
