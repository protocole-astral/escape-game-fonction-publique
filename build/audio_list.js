#!/usr/bin/node
import Audic from 'audic';
export default function get_audio_list() {
  let list = [];
  
  // [0] intro jeu
  list.push(new Audic('../mp3/1.mp3'));
  
  // ENIGME A (ville)
  // [1] premier message Camille, énoncé énigme A
  list.push(new Audic('../mp3/2.mp3'));
  // [2] A fail : rue en travaux 1
  list.push(new Audic('../mp3/3_a.mp3'));
  // [3] A fail : rue en travaux 2
  list.push(new Audic('../mp3/3_b.mp3'));
  // [4] A fail : ambulance
  list.push(new Audic('../mp3/4.mp3'));
  // [5] A fail : rue bloquée
  list.push(new Audic('../mp3/5.mp3'));
  // [6] A fail : rue bloquée éboueurs
  list.push(new Audic('../mp3/6.mp3'));
  // [7] A fail : trajet absurde
  list.push(new Audic('../mp3/7.mp3'));
  // [8] A sucess
  list.push(new Audic('../mp3/8.mp3'));
  
  // ENIGME B (concours)
  // [9] INDICE 0, énoncé énigme B
  list.push(new Audic('../mp3/9.mp3'));
  // [10] B fail
  list.push(new Audic('../mp3/10.mp3'));
  // ENIGME C (bâtiment)
  // [11] B success, énoncé énigme C
  list.push(new Audic('../mp3/11.mp3'));
  // [12] C fail : réunion
  list.push(new Audic('../mp3/12.mp3'));
  // [13] C fail : porte fermée
  list.push(new Audic('../mp3/13.mp3'));
  // [14] C fail : mur
  list.push(new Audic('../mp3/14.mp3'));
  // [15] C fail : pas d'escalier v1
  list.push(new Audic('../mp3/15_a.mp3'));
  // [16] C fail : pas d'escalier v2
  list.push(new Audic('../mp3/15_b.mp3'));
  // [17] C fail : pas d'escalier v3
  list.push(new Audic('../mp3/15_c.mp3'));
  // [18] C fail : pas d'escalier v4
  list.push(new Audic('../mp3/15_d.mp3'));
  // [19] C success
  list.push(new Audic('../mp3/16.mp3'));

  // ENIGME D (code emploi)
  // [20] INDICE 1, énoncé énigme D
  list.push(new Audic('../mp3/17.mp3'));
  // [21] D fail
  list.push(new Audic('../mp3/18.mp3'));
  
  // ENIGME E (bureau)
  // [22] D success, énoncé énigme E
  list.push(new Audic('../mp3/19.mp3'));
  // [23] E fail 1
  list.push(new Audic('../mp3/20_a.mp3'));
  // [24] E fail 2
  list.push(new Audic('../mp3/20_b.mp3'));
  // [25] E fail 3
  list.push(new Audic('../mp3/20_c.mp3'));
  // [26] E success
  list.push(new Audic('../mp3/21.mp3'));
  
  // ENIGME F (choix métier)
  // [27] INDICE 2, énoncé énigme F
  list.push(new Audic('../mp3/22.mp3'));
  // [28] F fail 1
  list.push(new Audic('../mp3/23_a.mp3'));
  // [29] F fail 2
  list.push(new Audic('../mp3/23_b.mp3'));
  // [30] Épilogue
  list.push(new Audic('../mp3/epilogue.mp3'));
  // [31] Musique
  list.push(new Audic('../mp3/musique.mp3')); 
  return list;
}
