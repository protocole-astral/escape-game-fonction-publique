import Audic from './audic.patched.mjs';

const audic = new Audic('c_est_cotelette_que_vous_comprenez_pas.mp3');

await audic.play();

audic.addEventListener('ended', () => {
	audic.destroy();
});