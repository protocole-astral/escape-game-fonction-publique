import vlcStatic from 'vlc-static';
import uniqueString from 'unique-string';
import getPort from 'get-port';
import execa from 'execa';
import got from 'got';
/**
An interface to VLC Media Player.

@example
```
import createVlc from '@richienb/vlc';

const vlc = await createVlc();

// Play audio
await vlc.command('in_play', {
    input: 'audio-file.mp3',
});

// Pause/resume audio
await vlc.command('pl_pause');
```
*/
export default async function createVlc() {
    const password = uniqueString();
    const ip = "127.0.0.1";
    const port = await getPort();
    if (!ip) {
        throw new Error('Unable to get internal IP address');
    }
    const address = `http://${ip}`;
    const instance = execa(vlcStatic(), ['--extraintf', 'http', '--intf', 'dummy', '--http-host', ip, '--http-port', port.toString(), '--http-password', password]);
    return {
        /**
        Get the current player status.
        */
        async info() {
            return got('requests/status.json', {
                port,
                password,
                responseType: 'json',
                prefixUrl: address,
                resolveBodyOnly: true,
            });
        },
        /**
        Get the current playlist information.
        */
        async playlist() {
            return got('requests/playlist.json', {
                port,
                password,
                responseType: 'json',
                prefixUrl: address,
                resolveBodyOnly: true,
            });
        },
        /**
        Execute a command.

        @param command The [command](https://wiki.videolan.org/VLC_HTTP_requests#Full_command_list) to execute.
        @param options The data to send with the command.
        */
        async command(command, options) {
            await got(`requests/status.json?${new URLSearchParams({
                command,
                ...options,
            }).toString().replace(/\+/g, '%20')}`, {
                port,
                password,
                prefixUrl: address,
                responseType: 'buffer',
            });
        },
        /**
        Kill the process.
        */
        kill() {
            instance.kill();
        },
    };
}
