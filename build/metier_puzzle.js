#!/usr/bin/node
console.log("metier_puzzle.js");
export default function metier_puzzle() {
  
  // Base puzzle object
  let metier_puzzle = {
    "signal_memory":[]
  };
  
  metier_puzzle.process = function(signal,game,matrix_port) {

    // Validate player sequence
    if (signal == '#'){
     
      // Player win 
      if (
        this.signal_memory[0] == '0'
        && this.signal_memory[1] == '1'
        && this.signal_memory[2] == '2'
      ) {
        matrix_port.write(Buffer.from('S'));
        this.signal_memory = [];
        return 1;
      
      // Player fail
      } else {
        matrix_port.write(Buffer.from('X'));
        this.signal_memory = [];
        // [28] F fail 1
        // [29] F fail 2
        return [28,29][Math.floor(Math.random()*2)];

      }

    // Memorize player choice in sequence
    } else if (signal > -1) {
      this.signal_memory.push(signal);
      matrix_port.write(Buffer.from('B'));
      console.log(this.signal_memory);
    }
    return 0;
  };

  return metier_puzzle;

}
