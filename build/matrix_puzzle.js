#!/usr/bin/node
console.log("matrix_puzzle.js");
export default function matrix_puzzle() {

  // Base puzzle object
  let matrix_puzzle = {
    "player_pos":64-8+10,
    "end_pos":64-2,
    "wall":[],
    "cant_pass":[],
    "closed_door":[],
    "crowd":[],
  };
  
  // Obstacles layouts
  let cant_pass_layout = [
    1,1,1,1,1,1,1,1, 
    0,0,0,0,0,0,0,0, 
    0,1,1,1,1,0,1,1, 
    0,0,0,0,0,0,0,0, 
    1,1,1,0,1,1,1,0, 
    0,0,0,0,0,0,0,0, 
    1,0,1,0,1,1,1,0, 
    0,0,0,0,0,0,0,0, 
  ];
  let crowd_layout = [
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,1,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
  ];
  let closed_door_layout = [
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,1,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
  ];
  let wall_layout = [
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,0,0,0,0,0,0, 
    0,0,1,0,0,1,0,0, 
  ];

  // Process obstacles layouts
  for (var i = 0; i < wall_layout.length; i++) {
    if (wall_layout[i]) matrix_puzzle.wall.push(i);
    if (cant_pass_layout[i]) matrix_puzzle.cant_pass.push(i);
    if (closed_door_layout[i]) matrix_puzzle.closed_door.push(i);
    if (crowd_layout[i]) matrix_puzzle.crowd.push(i);
  }

  // Puzzle process
  matrix_puzzle.process = function(signal,game,matrix_port,time) {
    // Define new player position
    let new_player_pos = (
      // move up
      signal == '2' && (this.player_pos - 10) - 8 > 8-10 ? (this.player_pos) - 8 :
      // move down
      signal == '8' && (this.player_pos - 10) + 8 < 64 ? (this.player_pos) + 8 :
      // move left
      signal == '4' && (this.player_pos - 10) % 8 ? (this.player_pos) - 1 :
      // move right
      signal == '6' && ((this.player_pos - 10) + 1) % 8 ? (this.player_pos) + 1 :
      // don't move
      this.player_pos
    );

    // Check player meet obstacle

    // Player meet wall
    if (this.wall.includes(new_player_pos-10)){
      return 14;
    // Player cant pass (ceiling or floor)
    } else if (this.cant_pass.includes(new_player_pos-10)){
      return [15,16,17,18][Math.floor(Math.random()*4)];
    // Player meet crowd
    } else if (this.crowd.includes(new_player_pos-10)){
      return 12;
    // Player meet closed door
    } else if (this.closed_door.includes(new_player_pos-10)){
      return 13;
    // Player cant move
    } else {
      this.player_pos = new_player_pos;
    }

    // Player win
    if (this.player_pos-10 == this.end_pos) {
      return 1;
    }
    
    // Update matrix display
    this.update_display(matrix_port,time);
  };
  
  // Puzzle visual output
  matrix_puzzle.update_display = function(matrix_port,time){
    matrix_port.write('R'+this.player_pos+10);
  }

  //matrix_port.write(Buffer.from('D'));
  // Return puzzle object
  return matrix_puzzle;
}
