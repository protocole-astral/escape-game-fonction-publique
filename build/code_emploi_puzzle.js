#!/usr/bin/node
console.log("code_emploi_puzzle.js");
export default function code_emploi_puzzle() {
  
  // Base puzzle object
  let code_emploi_puzzle = {
    "signal_memory":[]
  };
  
  code_emploi_puzzle.process = function(signal,game,matrix_port) {
    console.log(signal);

    // Validate player sequence
    if (signal == '#'){
      console.log("COUCOU");
     
      // Player win 
      if (
        this.signal_memory[0] == '1'
        && this.signal_memory[1] == '3'
      ) {
        matrix_port.write(Buffer.from('S'));
        this.signal_memory = [];
        return 1;
      
      // Player fail
      } else {
        matrix_port.write(Buffer.from('X'));
        this.signal_memory = [];
        // [21] D fail
        return 21;

      }

    // Memorize player choice in sequence
    } else if (signal > -1) {
      this.signal_memory.push(signal);
      matrix_port.write(Buffer.from('B'));
      console.log(this.signal_memory);
    }
    return 0;
  };

  return code_emploi_puzzle;

}
