#include <Adafruit_NeoPixel.h> // Librairie utilisée
int PIN = 9; // Broche du bandeau de led
int numPixel = 64; //nombre de led contenant le ruban
// On déclare la librairie avec les données précédente
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(numPixel,PIN, NEO_GRB + NEO_KHZ800);
void setup() {
	pixels.begin(); //On initialise la librairie
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
}

#define numberOfDigits 2
char theNumberString[numberOfDigits + 1];
int player_pos = 64-8;
int end_pos = 62;
int puzzle_state = 0;

void loop(){
  pixels.clear(); //On éteint les leds allumé
  char msg = Serial.read();
  if (msg == 'X') {
    for (int i = 0; i < numPixel; i++) {
      pixels.setPixelColor(i,pixels.Color(8,0,0));
    }
  }
  if (msg == 'Y') {
    for (int i = 0; i < numPixel; i++) {
      pixels.setPixelColor(i,pixels.Color(8,8,0));
    }
  }
  if (msg == 'S') {
    for (int i = 0; i < numPixel; i++) {
      pixels.setPixelColor(i,pixels.Color(0,8,0));
    }
  }
  if (msg == 'B') {
    for (int i = 0; i < numPixel; i++) {
      pixels.setPixelColor(i,pixels.Color(0,0,8));
    }
  }
  if (msg == 'D') {
    puzzle_state = !puzzle_state;
  }
  if (puzzle_state == 1) {
    if(msg == 'R') {
      for (int i = 0; i < numberOfDigits; i++) {
        theNumberString[i] = Serial.read();
      }
      theNumberString[numberOfDigits] = 0x00;
      char first_digit = theNumberString[0];
      char sec_digit = theNumberString[1];
      if (first_digit == '0' || first_digit == '1' || first_digit == '2' 
      || first_digit == '3' || first_digit == '4' || first_digit == '5' || first_digit == '6'
      || first_digit == '7' || first_digit == '8' || first_digit == '9') {
        if (sec_digit == '0' || sec_digit == '1' || sec_digit == '2' 
      || sec_digit == '3' || sec_digit == '4' || sec_digit == '5' || sec_digit == '6'
      || sec_digit == '7' || sec_digit == '8' || sec_digit == '9') {
        player_pos = atoi(theNumberString)-10;
      }
      }
    }
    pixels.setPixelColor(player_pos,pixels.Color(8,8,0));
    pixels.setPixelColor(end_pos,pixels.Color(0,8,0));
    pixels.show(); //On allume la led avec la couleur
    delay(1);
  } else {
    pixels.show(); //On allume la led avec la couleur
    delay(50);
  }
}
