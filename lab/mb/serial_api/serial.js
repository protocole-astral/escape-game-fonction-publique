function init () {
  if ("serial" in navigator) {
    console.log("serial supported");
    //find_port();
    navigator.serial.getPorts().then(function(ports){
      let port = ports[0];
      if (port) {
        setup_port(port);
      }
    });
  }
}

function setup_port(port){
  port.open({baudRate:9600}).then(foo=>{
    let textEncoder = new TextEncoderStream();
    let writableStreamClosed = textEncoder.readable.pipeTo(port.writable);
    let writer = textEncoder.writable.getWriter();
    listen(port,writer);
    button.addEventListener("click",function(){
      window.setInterval(function(){
        talk(port,"1",writer);
        setTimeout(function(){
          talk(port,"0",writer);
        },1000);
      },2000);
    });
  });
}

async function find_port(){
  const port = await navigator.serial.requestPort();
  console.log(port);
}

async function talk(port,message,writer){
  await writer.write(message);
};

async function listen(port,writer){
  //while (true) {
  //  const { value, done } = await reader.read();
  //  if (done) {
  //    // Allow the serial port to be closed later.
  //    reader.releaseLock();
  //    break;
  //  }
  //  // value is a Uint8Array.
  //  console.log(value);
  //}
  const textDecoder = new TextDecoderStream();
  const readableStreamClosed = port.readable.pipeTo(textDecoder.writable);
  const reader = textDecoder.readable.getReader();
  
  // Listen to data coming from the serial device.
  while (true) {
    const { value, done } = await reader.read();
    if (done) {
      // Allow the serial port to be closed later.
      reader.releaseLock();
      break;
    }
    // value is a string.
    if (value == 1 && talk_allowed) {
      talk_allowed = false;
      talk(port,["1","2","3"][talk_type],writer);
      talk_type = (talk_type + 1 )% 3;
      setTimeout(function(){
        talk_allowed = true;
        talk(port,"0",writer);
      },1000);
    }
    console.log(value);
  }

};

let talk_allowed = true;
let talk_type = 0;

init();

navigator.serial.addEventListener("connect", (e) => {
  // Connect to `e.target` or add it to a list of available ports.
});

navigator.serial.addEventListener("disconnect", (e) => {
  // Remove `e.target` from the list of available ports.
});

navigator.serial.getPorts().then((ports) => {
  // Initialize the list of available ports with `ports` on page load.
});

let button = document.getElementById("yo");
button.addEventListener("click", () => {
  const usbVendorId = 0x1a86;
  navigator.serial
    .requestPort({ filters: [{ usbVendorId }] })
    .then((port) => {
      if (port) setup_port(port);
    })
    .catch((e) => {
    });
});
