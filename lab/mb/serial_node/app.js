#!/usr/bin/node

const { SerialPort, ReadlineParser } = require('serialport')
const port = new SerialPort({ 
    path: '/dev/ttyUSB0', 
    baudRate: 9600,
});
const parser = new ReadlineParser();
port.pipe(parser);
parser.on('data', data =>{
  console.log('got word from arduino:', data[0]);
  if (data[0] == 1) {
    console.log("BOUTON APPUYE");
    port.write(Buffer.from(['1','2','3'][Math.floor(Math.random()*3)]));
    setTimeout(function(){
      port.write(Buffer.from('0'));
    },1000);
  }
});

//
//const { SerialPort } = require('serialport');
//const { Readline } = require('@serialport/parser-readline');
//
//const port = new SerialPort({
//  path: '/dev/ttyUSB0',
//  baudRate: 57600,
//  autoOpen: false,
//})
//const parser = port.pipe(new Readline({ delimiter: '\n' }));
// Read the port data
//port.on("open", () => {
//  console.log('serial port open');
//});parser.on('data', data =>{
//  console.log('got word from arduino:', data);
//});
/*

// BUFFER
function bufferFromBufferString(bufferStr) {
    return Buffer.from(
        bufferStr
            .replace(/[<>]/g, '') // remove < > symbols from str
            .split(' ') // create an array splitting it by space
            .slice(1) // remove Buffer word from an array
            .reduce((acc, val) => 
                acc.concat(parseInt(val, 16)), [])  // convert all strings of numbers to hex numbers
     )
}

// SERIAL
const { SerialPort } = require('serialport')

// Create a port
const port = new SerialPort({
  path: '/dev/ttyUSB0',
  baudRate: 57600,
  autoOpen: false,
})

//port.write('main screen turn on', function(err) {
//  if (err) {
//    return console.log('Error on write: ', err.message)
//  }
//  console.log('message written')
//})

port.open(function (err) {
  if (err) {
    return console.log('Error opening port: ', err.message)
  }
})

port.on('open',function(){

});
port.on('readable', function () {
  let buffer = port.read().toString();
  console.log('Data:', buffer[0]);
  const buf = Buffer.from(new String('1'));
  port.write("1");
})*/
