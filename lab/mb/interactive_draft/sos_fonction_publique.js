/* * * * * * * * * *
 * Dummy Mallette  *
 * * * * * * * * * */

// Signal
function dummy_signal(){
  // digicode
  if (digicode_signal && !digicode_down) digicode_memory.push(digicode_signal);
  digicode_signal = digicode_down ? digicode_signal : 0;
  //digicode_signal_log.textContent = digicode_signal + ' ' + digicode_memory;

  // start button
  start_button_signal = start_button_down ? start_button_signal : 0;
  //start_button_signal_log.textContent = start_button_signal;

  // clue 0 diode
  if (clue_0_diode_signal) {
    clue_0_diode_log.checked = true; 
    clue_0_diode_log.removeAttribute("disabled");
  } else {
    clue_0_diode_log.checked = false; 
    clue_0_diode_log.removeAttribute("checked");
    clue_0_diode_log.setAttribute("disabled","true");
  }
  
  // clue 1 diode
  if (clue_1_diode_signal) {
    clue_1_diode_log.checked = true; 
    clue_1_diode_log.removeAttribute("disabled");
  } else {
    clue_1_diode_log.checked = false; 
    clue_1_diode_log.removeAttribute("checked");
    clue_1_diode_log.setAttribute("disabled","true");
  }
  
  // clue 2 diode
  if (clue_2_diode_signal) {
    clue_2_diode_log.checked = true; 
    clue_2_diode_log.removeAttribute("disabled");
  } else {
    clue_2_diode_log.checked = false; 
    clue_2_diode_log.removeAttribute("checked");
    clue_2_diode_log.setAttribute("disabled","true");
  }
  
  // clue 0 button input
  clue_0_button_signal = clue_0_button_down ? clue_0_button_signal : 0;
  //clue_0_button_signal_log.textContent = clue_0_button_signal;
  
  // clue 1 button input
  clue_1_button_signal = clue_1_button_down ? clue_1_button_signal : 0;
  //clue_1_button_signal_log.textContent = clue_1_button_signal;
  
  // clue 2 button input
  clue_2_button_signal = clue_2_button_down ? clue_2_button_signal : 0;
  //clue_2_button_signal_log.textContent = clue_2_button_signal;
  
  // Matrix diode list
  for (var i = 0; i < matrix_diode_signal_list_log.length; i++){
    if (matrix_diode_signal_list[i]) {
      matrix_diode_signal_list_log[i].checked = true; 
      matrix_diode_signal_list_log[i].removeAttribute("disabled");
    } else {
      matrix_diode_signal_list_log[i].checked = false; 
      matrix_diode_signal_list_log[i].removeAttribute("checked");
      matrix_diode_signal_list_log[i].setAttribute("disabled","true");
    }
  }

  // speaker
  speaker_signal = Date.now() < speaker_signal_end ? speaker_audio : '';
  speaker_signal_log.textContent = speaker_signal;
}

// digicode
let digicode_signal = 0;
let digicode_memory = [];
let digicode_down = false;
let digicode_buttons_names = [
  '1','2','3',
  '4','5','6',
  '7','8','9',
  '*','0','#',
];
let digicode_signal_log;
function create_dummy_digicode(){
  let dummy_digicode = document.createElement("div");
  digicode_signal_log = document.createElement("div");
  dummy_digicode.appendChild(digicode_signal_log);
  for (var i = 0; i < digicode_buttons_names.length; i++){
    let button = document.createElement("button");
    let id = i+1;
    button.addEventListener("mousedown",function(){
      digicode_down = true;
      digicode_signal = id;
    });
    button.addEventListener("mouseup",function(){
      digicode_down = false;
    });
    button.textContent = digicode_buttons_names[i];
    dummy_digicode.appendChild(button);
    if (!((i+1)%3)){
      let br = document.createElement("br");
      dummy_digicode.appendChild(br);
    }
  }
  dummy_digicode.appendChild(document.createElement("br"));
  document.body.appendChild(dummy_digicode);
}

// speakers
let speaker_signal_log;
let speaker_audio = '';
let speaker_signal = '';
let speaker_signal_start = 0;
let speaker_signal_end = 0;

function create_dummy_speaker(){
  dummy_speaker = document.createElement("div");
  dummy_speaker.appendChild(document.createElement("div"));
  dummy_speaker.children[0].textContent = 'Speaker :';
  speaker_signal_log = document.createElement("div");
  dummy_speaker.appendChild(speaker_signal_log);
  dummy_speaker.appendChild(document.createElement("br"));
  document.body.appendChild(dummy_speaker);
}
function dummy_play_audio(audio_id){
  let str = dummy_audio_files[audio_id][0];
  let duration = dummy_audio_files[audio_id][1];
  speaker_signal_start = Date.now();
  speaker_signal_end = speaker_signal_start + duration;
  speaker_audio = str;
  message_memory = audio_id;
}
function dummy_play_unlisted_audio(str,duration){
  speaker_signal_start = Date.now();
  speaker_signal_end = speaker_signal_start + duration;
  speaker_audio = str;
}

// start button
let start_button_signal = 0;
let start_button_down = false;
let start_button_signal_log;
function create_dummy_start_button(){
  let dummy_start_button = document.createElement("div");
  start_button_signal_log = document.createElement("div");
  dummy_start_button.appendChild(start_button_signal_log);
  let button = document.createElement("button");
  button.textContent = "start";
  button.addEventListener("mousedown",function(){
    start_button_down = true;
    start_button_signal = 1;
  });
  button.addEventListener("mouseup",function(){
    start_button_down = false;
  });
  dummy_start_button.appendChild(button);
  document.body.appendChild(dummy_start_button);
}

// Clue diode 0
let clue_0_diode_signal = 0;
let clue_0_diode_log;
function create_dummy_clue_0_diode(){
  clue_0_diode_log = document.createElement("input");
  clue_0_diode_log.setAttribute("type","radio");
  document.body.appendChild(clue_0_diode_log);
}

// Clue button 0
let clue_0_button_signal = 0;
let clue_0_button_down = 0;
let clue_0_button_signal_log;
function create_dummy_clue_0_button(){
  let dummy_clue_0_button = document.createElement("div");
  clue_0_button_signal_log = document.createElement("div");
  dummy_clue_0_button.appendChild(clue_0_button_signal_log);
  let button = document.createElement("button");
  button.textContent = "clue";
  button.addEventListener("mousedown",function(){
    clue_0_button_down = true;
    clue_0_button_signal = 1;
  });
  button.addEventListener("mouseup",function(){
    clue_0_button_down = false;
  });
  dummy_clue_0_button.appendChild(button);
  document.body.appendChild(dummy_clue_0_button);
}

// Clue diode 1
let clue_1_diode_signal = 0;
let clue_1_diode_log;
function create_dummy_clue_1_diode(){
  clue_1_diode_log = document.createElement("input");
  clue_1_diode_log.setAttribute("type","radio");
  document.body.appendChild(clue_1_diode_log);
}

// Clue button 1
let clue_1_button_signal = 0;
let clue_1_button_down = 0;
let clue_1_button_signal_log;
function create_dummy_clue_1_button(){
  let dummy_clue_1_button = document.createElement("div");
  clue_1_button_signal_log = document.createElement("div");
  dummy_clue_1_button.appendChild(clue_1_button_signal_log);
  let button = document.createElement("button");
  button.textContent = "clue";
  button.addEventListener("mousedown",function(){
    clue_1_button_down = true;
    clue_1_button_signal = 1;
  });
  button.addEventListener("mouseup",function(){
    clue_1_button_down = false;
  });
  dummy_clue_1_button.appendChild(button);
  document.body.appendChild(dummy_clue_1_button);
}

// Clue diode 2
let clue_2_diode_signal = 0;
let clue_2_diode_log;
function create_dummy_clue_2_diode(){
  clue_2_diode_log = document.createElement("input");
  clue_2_diode_log.setAttribute("type","radio");
  document.body.appendChild(clue_2_diode_log);
}

// Clue button 2
let clue_2_button_signal = 0;
let clue_2_button_down = 0;
let clue_2_button_signal_log;
function create_dummy_clue_2_button(){
  let dummy_clue_2_button = document.createElement("div");
  clue_2_button_signal_log = document.createElement("div");
  dummy_clue_2_button.appendChild(clue_2_button_signal_log);
  let button = document.createElement("button");
  button.textContent = "clue";
  button.addEventListener("mousedown",function(){
    clue_2_button_down = true;
    clue_2_button_signal = 1;
  });
  button.addEventListener("mouseup",function(){
    clue_2_button_down = false;
  });
  dummy_clue_2_button.appendChild(button);
  document.body.appendChild(dummy_clue_2_button);
}

// Matrix
let matrix_cant_pass_layout = [
  1,1,1,1,1,1,1,1, 
  0,0,0,0,0,0,0,0, 
  0,1,1,1,1,0,1,1, 
  0,0,0,0,0,0,0,0, 
  1,1,1,0,1,1,1,0, 
  0,0,0,0,0,0,0,0, 
  1,0,1,0,1,1,1,0, 
  0,0,0,0,0,0,0,0, 
];
let matrix_meeting_layout = [
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,1,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
];
let matrix_closed_door_layout = [
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,1,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
];
let matrix_wall_layout = [
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,0,0,0,0,0,0, 
  0,0,1,0,0,1,0,0, 
];
let matrix_wall = [];
let matrix_cant_pass = [];
let matrix_closed_door = [];
let matrix_meeting = [];
let matrix_length = 8*8;
let matrix_signal = 0;
let matrix_diode_signal_log;
let matrix_diode_signal_list = [];
let matrix_diode_signal_list_log = [];
let player_pos = 64-8;
let end_pos = 64-2;
function creare_dummy_matrix() {
  let dummy_matrix = document.createElement("div");
  for (var i = 0; i < matrix_length; i++) {
    if (matrix_wall_layout[i]) matrix_wall.push(i);
    if (matrix_cant_pass_layout[i]) matrix_cant_pass.push(i);
    if (matrix_closed_door_layout[i]) matrix_closed_door.push(i);
    if (matrix_meeting_layout[i]) matrix_meeting.push(i);
    matrix_diode = document.createElement("input");
    matrix_diode.setAttribute("type","radio");
    dummy_matrix.appendChild(matrix_diode);
    matrix_diode_signal_list_log.push(matrix_diode);
    matrix_diode_signal_list.push(0);
    if (!((i+1)%8)){
      let br = document.createElement("br");
      dummy_matrix.appendChild(br);
    }
  }
  document.body.appendChild(dummy_matrix);
}

// Message memory
let message_memory = -1;

/* * * * * * * * * * * 
 * Dummy audio files *
 * * * * * * * * * * */

let dummy_audio_files = [
  ["Mallette : Vous venez de démarrer la mallette d’urgence. Nous avons reçu un signal de détresse émis par une personne s’appelant Camille. Vous avez vingt minutes pour l’aider, commencez par la fiche d’introduction.",10000],
  ["Camille : Bonjour, j'ai entendu parler de votre service d'assistance grâce à un ami qui travaille déjà dans la fonction publique. Il m'a dit que vous étiez les meilleurs pour aider les personnes souhaitant postuler. Bon je suis vraiment en retard pour déposer ma candidature ! Je ne connais pas le chemin pour rejoindre vos locaux. Pouvez-vous m'indiquer étape par étape le trajet le plus rapide pour que je puisse vous retrouver sans perdre une minute de plus ?",10000],
  //messages erreurs AMBULANCE enigme B déplacement 
  ["Camille : Une ambulance et les pompiers bloquent une partie de ce trajet !",5000],
  //messages erreurs TRAVAUX enigme B déplacement 
  ["Camille : Une des rues est bloquée pour les piétons je ne peux pas passer par là",5000],
  //messages erreurs ÉBOUEUR enigme B déplacement 
  ["Camille : Des éboueurs sont en train de travailler et bloquent ce trajet !",5000],
  ["Camille : Oui parfait, c’est là ! Merci beaucoup pour votre aide, je me dépêche de vous rejoindre.",5000],
  ["Camille : J'aurais besoin d'un premier coup de main. Vous savez, j'ai 18 ans et je viens tout juste d'obtenir mon bac. Je suis un peu perdue, je suis vraiment stressée et je ne sais pas par où commencer. Pourriez-vous m'expliquer comment je peux déposer mon dossier pour être admise dans la Fonction Publique ?",10000],
  ["Camille : Merci pour votre aide jusqu'ici. Je suis dans le hall du bâtiment, il y a beaucoup de monde, où se situe votre service ? Pouvez-vous me guider pour que je puisse trouver votre bureau au plus vite ?",10000],


// messages erreurs REUNION BROUHAHA enigme B déplacement 
["Camille : Il y a une grosse réunion dans l’open-space je ne vais pas pouvoir passer.",5000],

// messages erreurs PORTE FERME enigme B déplacement 
["Camille : Je suis devant le bureau des gestionnaires des retraites, je ne pense pas que ce soit le bon endroit.",5000],

// messages erreurs YA UN MUR enigme B déplacement 
["Camille :  Il y a un mur ici, vous êtes sûr que c’est par là ?",5000],

// messages erreurs Je peux pas monter par là ICI enigme B déplacement 
["Camille : Je ne peux pas monter par ici, l’escalier doit se trouver autre part",5000],

["Camille : “Je viens de trouver votre bureau. Mais c’est immense….”",5000],

["Camille : Mince, j’ai oublié de rentrer le code emploi référence dans mon dossier, il me reste peu de temps, je voulais postuler pour un métier qui permet d’aider les gens en difficulté, d’organiser des activités pour eux, j’ai aussi mon BAFA, je pense que cela pourrait être utile à mon dossier.",10000],
//messages erreur
["Camille :  je ne pense vraiment pas que cela soit fait pour moi…",5000],

["Camille : “Merci ! je me vois bien faire l’un de ces métiers, j’y vois plus clair. Ça y est je suis à l'intérieur, à qui dois-je m'adresser ? Il y a énormément de monde dans ce bureau !",5000],
//messages erreur 

["Camille : Je ne vous trouve pas dans ce coin là",5000],

["Camille : “Ça y est je suis là dans les temps ! j’ai enfin fini ce périple.",5000], 

// missing audio
//["Camille : “Merci pour votre aide, quel métier me conseillez-vous ?”",5000],

["Épilogue : Une année plus tard, Camille se trouvait en poste. Elle avait choisi de suivre les conseils de l'agent SOS fonction publique et avait intégré l'administration de son choix, un poste où elle se sentait à sa place. Son téléphone sonna, la sortant de sa concentration. C'était un appel de l'agent de Sos fonction publique qui l'avait accueillie lors de sa première visite. Après avoir échangé quelques minutes sur ses premières impressions quant à son poste et si elle voulait devenir mentor pour l’équipe SOS fonction publique, Camille raccrocha et se permit un sourire de satisfaction. Elle se souvenait de ses premières questions et de ses incertitudes. Aujourd'hui, elle savait qu'elle avait trouvé sa voie. Elle se sentait enfin accomplie et convaincue que chacun de ses gestes pouvaient contribuer à un changement positif.",10000]
];

/* * * * *
 * Game  *
 * * * * */
let game_status = 0;
let game_status_log = document.createElement("div");

function game(){

  // Game start
  if (game_status == 0) {
    digicode_memory = [];

    // Player(s) must press start button to start the game
    if (start_button_signal == 1) {
      game_status = 1;
      dummy_play_audio(0);

      // clear signal
      digicode_signal = 0;
    }
    
    // always empty digicode memory
    digicode_memory = [];
  }

  // Puzzle A
  if (game_status == 1) {
    
    // Player(s) must press '*' on digicode to listen Camille detress signal
    if (digicode_signal == 12) {
      game_status = 2;
      dummy_play_audio(1);

      // clear digicode_signal
      digicode_signal = 0;
    }
    
    // always empty digicode memory
    digicode_memory = [];
  }

  // Puzzle B (city)
  if (game_status == 2) {
    
    // Player(s) must send 30 or 03 on digicode to help Camille find its way in the city
    if ( 
      (
        (digicode_memory[0] == 11 && digicode_memory[1] != 11)
        || (digicode_memory[0] == 3 && digicode_memory[1] != 3)
      )
      && (
        (digicode_memory[1] == 11 && digicode_memory[0] != 11)
        || (digicode_memory[1] == 3 && digicode_memory[0] != 3)
      )
    ) {
      game_status = 3;
      dummy_play_audio(5);

      // Next game status and Camille message is timed
      window.setTimeout(function(){
        game_status = 4;
        clue_0_diode_signal = 1;
        dummy_play_audio(6);
      },dummy_audio_files[5][1]);
      
      // clear digicode_signal
      digicode_signal = 0;
    } else if (digicode_memory.length > 1 && !digicode_memory.includes(12)){

    // Player(s) fail
      
      let error_clues = []
      if (digicode_memory.includes(1)) error_clues.push(1);
      if (digicode_memory.includes(2)) error_clues = error_clues.concat([0,1]);
      if (digicode_memory.includes(4)) error_clues = error_clues.concat([1,2]);
      if (digicode_memory.includes(5)) error_clues = error_clues.concat([2]);
      let error_clues_audio = [
        dummy_audio_files[2],
        dummy_audio_files[3],
        dummy_audio_files[4]
      ];
      let clue = error_clues_audio[
        Math.floor(
          Math.random()*error_clues_audio.length
        )
      ];
      if (digicode_memory.includes(11) || digicode_memory.includes(10) || !error_clues.length || digicode_memory[0] == digicode_memory[1]) {
      
        // Giving two times same digit
        // TODO
        dummy_play_unlisted_audio("missing audio file : trajet absurde",5000);
      } else {
        dummy_play_unlisted_audio(clue[0],clue[1]);
      }
      // clear digicode_signal
      digicode_signal = 0;
    }
    
    // empty digicode memory after two entries
    digicode_memory = digicode_memory.length > 1 ? [] : digicode_memory;
  }
  
  // Waiting status
  if (game_status == 3) {
    // always empty digicode memory
    digicode_memory = [];
  }
  
  // Puzzle C
  if (game_status == 4) {
    // Player(s) must send TODO on digicode to give Camille right information
    if ( 
      digicode_memory[0] == 1
      && digicode_memory[1] == 2
      && digicode_memory[2] == 3
      && digicode_memory[3] == 4
    ) {
      game_status = 5;
      dummy_play_audio(7);
    } else if (digicode_memory.length > 3){

      // Player(s) fail : play TODO
      dummy_play_unlisted_audio("missing audio file : réponse fausse",5000);
    }

    digicode_memory = digicode_memory.length > 3 ? [] : digicode_memory;
  }
  
  // Puzzle D (building)
  if (game_status == 5){
    
    // clear matrix data
    for (var i = 0; i < matrix_diode_signal_list.length; i++) {
      matrix_diode_signal_list[i] = 0;
    }

    // player controls
    let previous_player_pos = player_pos;

    // player move top
    if (
      digicode_signal == 2
      && player_pos - 8 > 0
    ) {
      player_pos-=8;

      // clear digicode_signal
      digicode_signal = 0;
    }

    // player move bottom
    if (
      digicode_signal == 8
      && player_pos + 8 < 64
    ) {
      player_pos+=8;

      // clear digicode_signal
      digicode_signal = 0;
    }
    
    // player move left
    if (
      digicode_signal == 4
      && player_pos%8
    ) {
      player_pos--;
      
      // clear digicode_signal
      digicode_signal = 0;
    }
    
    // player move right
    if (
      digicode_signal == 6
      && (player_pos+1)%8
    ) {
      player_pos++;
      
      // clear digicode_signal
      digicode_signal = 0;
    }

    // Is player metting obstacle ?

    if (matrix_wall.includes(player_pos)){
      player_pos = previous_player_pos;
      dummy_play_unlisted_audio(dummy_audio_files[10][0],dummy_audio_files[10][1]);
    }
    if (matrix_cant_pass.includes(player_pos)){
      player_pos = previous_player_pos;
      dummy_play_unlisted_audio(dummy_audio_files[11][0],dummy_audio_files[11][1]);
    }
    if (matrix_meeting.includes(player_pos)){
      player_pos = previous_player_pos;
      dummy_play_unlisted_audio(dummy_audio_files[8][0],dummy_audio_files[8][1]);
    }
    if (matrix_closed_door.includes(player_pos)){
      player_pos = previous_player_pos;
      dummy_play_unlisted_audio(dummy_audio_files[9][0],dummy_audio_files[9][1]);
    }

    if (player_pos == end_pos){
      game_status = 3;
      dummy_play_audio(12);
      window.setTimeout(function(){
        for (var i = 0; i < matrix_diode_signal_list.length; i++) {
          matrix_diode_signal_list[i] = 0;
        }
        game_status = 6;
        clue_1_diode_signal = 1;
        dummy_play_audio(13);
      },dummy_audio_files[12][1]);
    }

    // show player
    matrix_diode_signal_list[player_pos] = 1; 
    
    // show end
    matrix_diode_signal_list[end_pos] = 1;
    
    // always empty digicode memory
    digicode_memory = [];
  }
  
  // Puzzle E
  if (game_status == 6) {
    // Player(s) must send TODO on digicode to give Camille right information
    if ( 
      digicode_memory[0] == 1
      && digicode_memory[1] == 2
      && digicode_memory[2] == 3
      && digicode_memory[3] == 4
    ) {
      game_status = 7;
      dummy_play_audio(15);
      
      //// Next game status and Camille message is timed
      //window.setTimeout(function(){
      //  game_status = 7;
      //  clue_2_diode_signal = 1;
      //  dummy_play_audio(16);
      //},dummy_audio_files[5][1]);
    } else if (digicode_memory.length > 3){

      // Player(s) fail : play TODO
      dummy_play_unlisted_audio(dummy_audio_files[14][0],dummy_audio_files[14][1]);
    }

    digicode_memory = digicode_memory.length > 3 ? [] : digicode_memory;
  }
  
  // Puzzle F
  if (game_status == 7) {
    // Player(s) must send TODO on digicode to give Camille right information
    if ( 
      digicode_memory[0] == 3
      && digicode_memory[1] == 4
    ) {
      game_status = 3;
      dummy_play_audio(17);
      
      // Next game status and Camille message is timed
      window.setTimeout(function(){
        game_status = 8;
        clue_2_diode_signal = 1;
        //dummy_play_audio(18);
        // TODO
        dummy_play_unlisted_audio("missing audio file",5000);
      },dummy_audio_files[17][1]);
    } else if (digicode_memory.length > 1){

      // Player(s) fail : play TODO
      dummy_play_unlisted_audio(dummy_audio_files[16][0],dummy_audio_files[16][1]);
    }

    digicode_memory = digicode_memory.length > 1 ? [] : digicode_memory;
  }
  
  // Puzzle G
  if (game_status == 8) {
    // Player(s) must send TODO on digicode to give Camille right information
    if ( 
      digicode_memory[0] == 1
      && digicode_memory[1] == 2
      && digicode_memory[2] == 3
      && digicode_memory[3] == 4
    ) {
      game_status = 9;
      dummy_play_audio(18);
      
      //// Next game status and Camille message is timed
      //window.setTimeout(function(){
      //  game_status = 7;
      //  clue_2_diode_signal = 1;
      //  dummy_play_audio(16);
      //},dummy_audio_files[5][1]);
    } else if (digicode_memory.length > 3){

      // Player(s) fail : play TODO
      dummy_play_unlisted_audio(dummy_audio_files[14][0],dummy_audio_files[14][1]);
    }

    digicode_memory = digicode_memory.length > 3 ? [] : digicode_memory;
  }
  
  // Puzzle H
  if (game_status == 9) {
    matrix_diode_signal_list[Math.floor(Math.random()*matrix_diode_signal_list.length)] = Math.random() > 0.5 ? 1 : 0;
  }

  // Play clue 0
  if (clue_0_button_signal && !speaker_signal.length && clue_0_diode_signal) {
    dummy_play_audio(6);
    clue_0_button_signal = 0;
  }
  
  // Play clue 1
  if (clue_1_button_signal && !speaker_signal.length && clue_1_diode_signal) {
    dummy_play_audio(13);
    clue_1_button_signal = 0;
  }

  // Message memory
  if (message_memory > -1 && !speaker_signal.length && digicode_signal == 12) {
    dummy_play_audio(message_memory);
  }

  // Clear digicode memory with '#'
  if (digicode_memory.includes(12)){
    digicode_memory = [];
  }
}

/* * * * *
 * Boot  *
 * * * * */

function setup(){
  create_dummy_start_button();
  create_dummy_digicode();
  create_dummy_speaker();
  create_dummy_clue_0_diode();
  create_dummy_clue_0_button();
  create_dummy_clue_1_diode();
  create_dummy_clue_1_button();
  create_dummy_clue_2_diode();
  create_dummy_clue_2_button();
  creare_dummy_matrix();
  document.body.appendChild(game_status_log);
}

function main(){
  game();
  //game_status_log.textContent = "Game status : "+game_status;
  dummy_signal();
}

setup();
window.setInterval(main,17);

// TODO réécouter le dernier message
