/*const annotate = RoughNotation.annotate;
// const e = document.querySelector('#myElement');


*/

window.PagedConfig = {
    auto: false, // disable automatic script execution on document load
};

document.addEventListener("DOMContentLoaded", function() {
    init();
}, false);

function init() {
    window.PagedPolyfill.preview();

    setTimeout(() => {
        const annotate = RoughNotation.annotate;
        var els = document.querySelectorAll(".clue.circle");
        for (var i = 0; i < els.length; i++) {
            var iterations = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            const annotation = annotate(els[i], {
                type: 'circle',
                color: 'var(--color_2)',
                padding: 8,
                iterations: iterations,
                strokeWidth: 1
            });
            annotation.show();
        }

        var els_underline = document.querySelectorAll(".clue.underline");
        for (var j = 0; j < els_underline.length; j++) {
            var iterations = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            const annotation = annotate(els_underline[j], {
                type: 'underline',
                color: 'var(--color_2)',
                iterations: iterations,
                padding: 1,
                strokeWidth: 1
            });
            annotation.show();
        }

        var els_box = document.querySelectorAll(".clue.box");
        for (var j = 0; j < els_box.length; j++) {
            var iterations = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            const annotation = annotate(els_box[j], {
                type: 'box',
                color: 'var(--color_2)',
                iterations: iterations,
                padding: 2,
                strokeWidth: 2
            });
            annotation.show();
        }

        var els_highlight = document.querySelectorAll(".clue.highlight");
        for (var j = 0; j < els_highlight.length; j++) {
            var iterations = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            const annotation = annotate(els_highlight[j], {
                type: 'highlight',
                color: 'var(--color_2_transparent)',
                iterations: iterations,
                padding: 0,
                strokeWidth: 1
            });
            annotation.show();
        }

        var els_bracket = document.querySelectorAll(".clue.bracket");
        for (var j = 0; j < els_bracket.length; j++) {
            var iterations = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            const annotation = annotate(els_bracket[j], {
                type: 'bracket',
                color: 'var(--color_2)',
                iterations: iterations,
                padding: 3,
                strokeWidth: 1,
                brackets: "left"
            });
            annotation.show();
        }

        var els_crossed_off = document.querySelectorAll(".clue.crossed-off");
        for (var j = 0; j < els_crossed_off.length; j++) {
            var iterations = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            const annotation = annotate(els_crossed_off[j], {
                type: 'crossed-off',
                color: 'var(--color_2)',
                iterations: iterations,
                padding: 0,
                strokeWidth: 2,
            });
            annotation.show();
        }


    }, "2000");
    
    // toc
    class handlers extends Paged.Handler {
        constructor(chunker, polisher, caller) {
          super(chunker, polisher, caller);
        }
        beforeParsed(content) {
          createToc({
            content: content,
            tocElement: "#toc",
            titleElements: [".list_in_toc"],
          });
        }
    }
    Paged.registerHandlers(handlers);
}

function createToc(config) {
  const content = config.content;
  const tocElement = config.tocElement;
  const titleElements = config.titleElements;

  let tocElementDiv = content.querySelector(tocElement);
  let tocUl = document.createElement("ul");
  tocUl.id = "list-toc-generated";
  tocElementDiv.appendChild(tocUl);

  // add class to all title elements
  let tocElementNbr = 0;
  for (var i = 0; i < titleElements.length; i++) {
    let titleHierarchy = i + 1;
    let titleElement = content.querySelectorAll(titleElements[i]);

    titleElement.forEach(function (element) {
      // add classes to the element
      element.classList.add("title-element");
      element.setAttribute("data-title-level", titleHierarchy);

      // add id if doesn't exist
      tocElementNbr++;
      idElement = element.id;
      if (idElement == "") {
        element.id = "title-element-" + tocElementNbr;
      }
      let newIdElement = element.id;
    });
  }

  // create toc list
  let tocElements = content.querySelectorAll(".title-element");

  for (var i = 0; i < tocElements.length; i++) {
    let tocElement = tocElements[i];

    let tocNewLi = document.createElement("li");

    // Add class for the hierarcy of toc
    tocNewLi.classList.add("toc-element");
    tocNewLi.classList.add(
      "toc-element-level-" + tocElement.dataset.titleLevel
    );

    // Keep class of title elements
    let classTocElement = tocElement.classList;
    for (var n = 0; n < classTocElement.length; n++) {
      if (classTocElement[n] != "title-element") {
        tocNewLi.classList.add(classTocElement[n]);
      }
    }

    // Create the element
    tocNewLi.innerHTML =
      '<a href="#' + tocElement.id + '">' + tocElement.innerHTML + "</a>";
    tocUl.appendChild(tocNewLi);
  }
}